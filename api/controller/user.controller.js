const seq = require("../model");
const User = seq.user;
const SocialPost = seq.socialPost;
const Tag = seq.tag;
const Feedback = seq.feedback;
const Like = seq.like;
const Comment = seq.comment;
const BlockPost = seq.blockPost;


const Op = seq.Sequelize.Op;
const sequelize = seq.sequelize;
const db = require("../config/db.config");
const bcrypt = require("bcrypt");
const __AUTHTOKEN = process.env.authkey;
var jwt = require("jsonwebtoken");
// console.log(__AUTHTOKEN)
const notification=require("../controller/notification.controller");


// Create and Save a new User
exports.addUser = async (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    const t = await sequelize.transaction(); // use for async operation
    try {
        if (req.body.email || req.body.phone) {
            if (!req.body.email) req.body.email = null;
            if (!req.body.phone) req.body.phone = null;
            if (req.body.phone && req.body.phone.length != 10){
                res.json({ status: 0, message: "Phone must be 10 Digit" });
            }else{
                var newUser = await User.create(req.body, { transaction: t });
                var userCode = ('000' + newUser.id).slice(-4);
                await User.update({ 'userCode': ('helpApp' + userCode) }, { returning: true, where: { 'id': newUser.id }, transaction: t });
                await t.commit();
                // res.statusCode=201; 
                res.status(201).json({ status: 1, message: "User Added Successfully.", data: newUser });
            }
        } else {
            res.json({ status: 0, message: "Please Provide Proper Data." });
        }

    } catch (error) {
        await t.rollback();
        // console.log(error.errors[0].validatorKey)
         if(error.errors[0].validatorKey=='not_unique'){
            errmsg="Email or Phone Already in Use";
        }else{
            errmsg='Error Occured';
        }        
        res.status(400).json({ status: -1, message: errmsg });
    }
};

//login user
exports.login = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    let sql = `select id,profileType,name,email,phone,password,profilePicture from tbl_user where phone='${req.body.phone}' or email='${req.body.email}' and userType='user' `;

    db.query(sql, (err, result) => {
        if (err) {
            res.status(400).json({status: -1, message: "error occured",  });
        }
        else if (result.length > 0 && result[0].isEnable == 0) {
            // res.statusCode=401; 
            res.status(401).json({ status: 0, message: "User Account Is not Active",  });
        }
        else if (result.length > 0 && result[0].isVerify == 0) {
            res.status(401).json({ status: 0, message: "User Account Is not Verified",  });
        }else if (result.length > 0 && result[0].isDeleted == 1) {
            res.status(401).json({ status: 0, message: "Account Removed , Contact Customer Support",  });
        }
        else if (result.length == 0) {
            res.status(404).json({ status: 0, message: "No User record for this Credential",  });
        } else {
            bcrypt.compare(req.body.password, result[0].password, function (hashError, hashRes) {
                if (hashError) {
                    res.status(400).json({status: -1, message: "error occured",  });
                } else if (hashRes == true) {
                    let token = jwt.sign(
                        { userId: result[0].id },
                        __AUTHTOKEN
                    );
                    // res.json({ status: 1, message: "Login Successful", data: result });
                    result[0].password = undefined;
                    res.json({ status: 1, message: "Login Successful",data:result, token });
                } else {
                    res.status(401).json({ status: 0, message: "Incorrect Credentials" });
                }
            });
        }
    });
};

//Edit User
exports.editUser = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    if (req.params.id) {
        var id = req.params.id;
    }else{
        var id = req.userId;
    }

    if (req.body.password) {
        res.status(400).json({ status: 0, message: "password can not be updated" });
    } else {
        User.update(req.body, { returning: true, where: { id } })
            .then(data => {
                res.json({
                    status: 1,
                    message: "User Updated"
                });
            })
            .catch(err => {
                res.status(400).json({ status: -1, message: "error occured"  });
            });
    }

};
//Change Profile Type
exports.editProfileType = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    var id = req.params.id;

        User.update({profileType:req.body.profileType}, { returning: true, where: { id } })
            .then(data => {
                res.json({
                    status: 1,
                    message: "User Profile Type added"
                });
            })
            .catch(err => {
                res.status(400).json({ status: -1, message: "error occured"  });
            });

};

exports.adddProfilePic = async (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    // const id = req.params.id;
    const id = req.userId;
    let profileImage;
    let uploadPath;

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    var date = Date.now();
    profileImage = req.files.profileImage;
    uploadPath = process.cwd() + '/uploads/' + date + profileImage.name;
    var fullUrl = req.protocol + '://' + req.get('host');
    var reqData = { profilePicture: date + profileImage.name };
    profileImage.mv(uploadPath, function (err) {
        if (err) {
            return res.status(500).send(err);
        } else {
            User.update(reqData, { returning: true, where: { id } })
                .then((data) => {
                    res.json({ status: 1, message: "ProfilePic Updated Successfully.", image: fullUrl + `/image/${date+profileImage.name}` });
                }).catch((err1) => {
                    res.status(400).json({ status: -1, message: "Failed to save ProfilePic!" });
                });
        }
    });
}

//Generate OTP
exports.otp = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    // const id = req.params.id;
    const email=req.body.email;
    const phone=req.body.phone;
    var valOtp = Math.floor(1000 + Math.random() * 9000);
    let updateValues = { otp: valOtp };
    User.update(updateValues, { returning: true, where: {
            [Op.or]: [{email: email }, { phone: phone }]
    }})
    .then(data => {
            res.status(400).json({
                status: 1,
                message: "OTP Generated Successfully",
                otp: valOtp
            });
    })
    .catch(err => {res.status(400).json({ status: -1, message: "error occured" });
    });
};

//verify OTP
exports.verifyotp = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");

    User.findOne({ where: { [Op.or]: [{email: req.body.email }, { phone: req.body.phone }] } })
    .then(result => {
        if(result.otp==req.body.otp){
            User.update({isVerify:1,isEnable: 1}, { returning: true, where: {[Op.or]: [{email: req.body.email }, { phone: req.body.phone }] } })
            .then(data => {
                res.json({
                    status: 1, message: "OTP Valiated"
                });
            })
            .catch(err => {
                res.status(400).json({ 
                    status: -1, message: "error occured" 
                });
            });
        }else{
            res.status(400).json({
                status: -1,
                message: "InValid OTP"
            });
        }
    }).catch(err => {
        res.status(400).json({
            status: -1,
            message: "error occured"
            
        });
    });
}

//forgot password
exports.forgotPassword =  (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    User.findOne({ where: { [Op.or]: [{email: req.body.email }, { phone: req.body.phone }] } })
    .then(result => {
        if(result!=null){
        if(result.otp==req.body.otp){
            User.update({password:req.body.password}, { returning: true, where: {[Op.or]: [{email: req.body.email }, { phone: req.body.phone }] } })
            .then(data => {
                res.json({
                    status: 1, message: "OTP Valiated and user Password updated Successfully"
                });
            })
            .catch(err => {
                res.status(400).json({ 
                    status: -1, message: "error occured"
                });
            });
        }else{
            res.status(400).json({
                status: -1,
                message: "InValid OTP"
            });
        }
    }else{
        res.status(400).json({ 
            status: -1, message: "USER not Exist"
        });
    }
    }).catch(err => {
        res.status(400).json({
            status: -1,
            message: "error occured"
           
        });
    });
};

//Change password
exports.changePassword = async (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    // const id = req.params.id;
    const id = req.userId;
    if (req.body.oldpassword && req.body.password) {
        User.findByPk(id)
        .then(result => {
            bcrypt.compare(req.body.oldpassword, result.password, function (hashError, hashRes) {
                if (hashError) {
                    res.status(400).json({status: -1, message: "error occured" });
                } else if (hashRes == true) {
                    User.update({ password: req.body.password }, { returning: true, where: { id } })
                        .then(data => {
                            let notifyData={ userId: id,notificationMsg: "Your Password Changed Successfully",notificationevent:"changePassword" };
                             notification.addNotification(notifyData);
                            res.json({
                                status: 1,
                                message: "Password Changed"
                            });
                        })
                        .catch(err => {
                            res.status(400).json({ status: -1, message: "error occured" });
                        });
                } else {
                    res.json({ status: 0, message: "OLD password Mismatched" });
                }
            });

        }).catch(err => {
            res.status(400).json({
        status: -1,
                message: "error occured",
                error: err
            });
        });

    } else {
        res.json({ status: 0, message: "OLD & NEW Password Required" });
    }
};


//Get All User
exports.getAllUser = (req, res) => {
    User.findAll({where:{userType:'User',isDeleted:0},
    attributes: ['id','name','email', 'phone','profilePicture','address','gender','profession','dob','desc','signupMethod','profileType','userType','isEnable','isVerify','createdBy','createdAt','updatedBy','updatedAt'] //attributes added  
    }).then(result => {
        res.json({
            status: 1,
            message: "All User fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured"});
    });
};

//Remove User
exports.removeUser = async (req, res) => {
    const id = req.params.id;

    const t = await sequelize.transaction();
    try {
      await Feedback.destroy({ where: { userId: id } },{ transaction: t });
      await BlockPost.destroy({ where: { userId: id } },{ transaction: t });
      await Like.destroy({ where: { userId: id } },{ transaction: t });
      await Comment.destroy({ where: { userId: id } },{ transaction: t });
      await Tag.destroy({ where: { userId: id } },{ transaction: t });
      await SocialPost.destroy({ where: { userId: id } },{ transaction: t });
      await User.destroy({ where: { id: id } }, { transaction: t });

      await t.commit();
      await res.json({
        status: 1,
        message: "User deleted Successfully.",
      });
    } catch (error) {
      await t.rollback();
      await res.status(400).json({
        status: -1,
        message: "Error occured while deleting User"
        
      });
    }
};

//Delete User
exports.deleteUser =  (req, res) => {
    const id = req.userId;

    var deleteuser = { isDeleted: 1 };
    User.update(deleteuser, { returning: true, where: { id } })
        .then(data => {
            res.json({
                status: 1,
                message: "User Deleted"
            });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured" });
        });
       
}