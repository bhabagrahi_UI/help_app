const db = require("../model/index");
const Notification = db.notification;
const Op = db.Sequelize.Op;



//Add Notification
exports.addNotification = (data) => {
    // req.body.userId=req.userId;
    Notification.create(data);
    // Notification.create(req.body).then(data => {
    //     res.json({ status: 1, message: "Notification Added Successfully." });
    // }).catch(err => {
    //     res.status(400).json({status: -1, message: "Error occured while Adding Notification." });
    // });
    console.log(data);
};

//Get All Notification
exports.getAllNotificationByUserId = (req, res) => {
    userId=req.userId;
    Notification.findAll({
        where: { userId: userId },
        order: [["createdAt", "DESC"]],
      }).then(result => {
        res.json({
            status: 1,
            message: "All Notification fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured" });
    });
};

//Get Notification by Id
// exports.getNotificationById = (req, res) => {
//     const id = req.params.id;

//     Notification.findByPk(id)
//         .then(result => {
//             res.json({
//                 status: 1,
//                 message: "Notification by Id fetched successfully",
//                 data: result
//             });
//         }).catch(err => {
//             res.json({
//                 status: -1,
//                 message: "error occured",
//                 error: err
//             });
//         });
// };

// ///Update Notification
// exports.updateNotification = (req, res) => {
//     const id = req.params.id;
//     req.body.userId=req.userId;
//     Notification.update(req.body, { returning: true, where: { id } })
//         .then(data => {
//             res.json({
//                 status: 1,
//                 message: "Notification Updated"
//             });
//         })
//         .catch(err => {
//             res.status(400).json({status: -1, message: "error occured" });
//         });
// };

//Delete Notification
exports.deleteNotification = (req, res) => {
    const id = req.params.id;

    Notification.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.json({
                    status: 1,
                    message: "Notification was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Notification with id=${id}. Maybe Notification was not found!`
                });
            }
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured" });
        });
};

exports.clearNotification = (req, res) => {
    const userId = req.userId;

    Notification.destroy({
        where: { userId: userId }
    })
        .then(clear => {
                res.json({
                    status: 1,
                    message: "Notification Cleared successfully!"
                });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured" });
        });
};