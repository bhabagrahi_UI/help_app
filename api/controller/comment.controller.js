const db = require("../model/index");
const Comment = db.comment;
const Op = db.Sequelize.Op;

const sequelize = db.sequelize;



//Add Comment
exports.addComment = (req, res) => {
    req.body.userId=req.userId;
    Comment.create(req.body).then(data => {
        res.json({ status: 1, message: "Comment Added Successfully." });
    }).catch(err => {
        res.status(400).json({status: -1, message: "Error occured while Adding Comment.", error: err });
    });
};

//Get All Comment
exports.getAllComment = (req, res) => {
    Comment.findAll().then(result => {
        res.json({
            status: 1,
            message: "All Comment fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

//Get Comment by Id
exports.getCommentById = (req, res) => {
    const id = req.params.id;

    Comment.findByPk(id)
        .then(result => {
            res.json({
                status: 1,
                message: "Comment by Id fetched successfully",
                data: result
            });
        }).catch(err => {
            res.status(400).json({
        status: -1,
                message: "error occured",
                error: err
            });
        });
};

///Update Comment
exports.updateComment = (req, res) => {
    const id = req.params.id;
    req.body.userId=req.userId;
    Comment.update(req.body, { returning: true, where: { id } })
        .then(data => {
            res.json({
                status: 1,
                message: "Comment Updated"
            });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};

//Delete Comment
exports.deleteComment = (req, res) => {
    const id = req.params.id;

    Comment.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.json({
                    status: 1,
                    message: "Comment was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Comment with id=${id}. Maybe Comment was not found!`
                });
            }
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};