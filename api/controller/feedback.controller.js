const db = require("../model/index");
const Feedback = db.feedback;
const Op = db.Sequelize.Op;

const sequelize = db.sequelize;


//Add Feedback
exports.addFeedback = (req, res) => {
    req.body.userId=req.userId;
    Feedback.create(req.body).then(data => {
        res.json({ status: 1, message: "Feedback Added Successfully." });
    }).catch(err => {
        res.status(400).json({status: -1, message: "Error occured while Adding Feedback.", error: err });
    });
};

//Get All Feedback
exports.getAllFeedback = (req, res) => {
    Feedback.findAll().then(result => {
        res.json({
            status: 1,
            message: "All Feedback fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

//Get Feedback by Id
exports.getFeedbackById = (req, res) => {
    const id = req.params.id;

    Feedback.findByPk(id)
        .then(result => {
            res.json({
                status: 1,
                message: "Feedback by Id fetched successfully",
                data: result
            });
        }).catch(err => {
            res.status(400).json({
        status: -1,
                message: "error occured",
                error: err
            });
        });
};

///Update Feedback
exports.updateFeedback = (req, res) => {
    const id = req.params.id;
    req.body.userId=req.userId;
    Feedback.update(req.body, { returning: true, where: { id } })
        .then(data => {
            res.json({
                status: 1,
                message: "Feedback Updated"
            });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};

//Delete Feedback
exports.deleteFeedback = (req, res) => {
    const id = req.params.id;

    Feedback.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.json({
                    status: 1,
                    message: "Feedback was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Feedback with id=${id}. Maybe Feedback was not found!`
                });
            }
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};