const db = require("../model/index");
const Like = db.like;
const Op = db.Sequelize.Op;



//Add Like
exports.addLike = (req, res) => {
    req.body.userId=req.userId;
    Like.create(req.body).then(data => {
        res.json({ status: 1, message: "Like Added Successfully." });
    }).catch(err => {
        res.status(400).json({status: -1, message: "Error occured while Adding Like.", error: err });
    });
};

//Get All Like
exports.getAllLike = (req, res) => {
    Like.findAll().then(result => {
        res.json({
            status: 1,
            message: "All Like fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

//Get Like by Id
exports.getLikeById = (req, res) => {
    const id = req.params.id;

    Like.findByPk(id)
        .then(result => {
            res.json({
                status: 1,
                message: "Like by Id fetched successfully",
                data: result
            });
        }).catch(err => {
            res.status(400).json({
        status: -1,
                message: "error occured",
                error: err
            });
        });
};

///Update Like
exports.updateLike = (req, res) => {
    const id = req.params.id;
    req.body.userId=req.userId;
    Like.update(req.body, { returning: true, where: { id } })
        .then(data => {
            res.json({
                status: 1,
                message: "Like Updated"
            });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};

//Delete Like
exports.deleteLike = (req, res) => {
    const id = req.params.id;

    Like.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.json({
                    status: 1,
                    message: "Like was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Like with id=${id}. Maybe Like was not found!`
                });
            }
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};