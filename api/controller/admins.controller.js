const seq = require("../model");
const User = seq.user;

const Op = seq.Sequelize.Op;
const sequelize = seq.sequelize;
const db = require("../config/db.config");
const bcrypt = require("bcrypt");
const __AUTHTOKEN = process.env.authkey;
var jwt = require("jsonwebtoken");

exports.adminLogin = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    User.findOne({
        where:{
            [Op.and]:[{email: req.body.email,userType: { [Op.ne]: "user" }},                
        ]}
    })
    .then(result=>{
        if(result!=null){
            bcrypt.compare(req.body.password, result.password, function (hashError, hashRes) {
                if (hashError) {
                    res.status(400).json({status: -1, message: "error occured", error: err });
                } else if (hashRes == true) {
                    console.log(result)
                    let token = jwt.sign(
                        { userId: result.id, type: result.userType },
                        __AUTHTOKEN
                    );
                    res.json({ status: 1, message: "Admin Login Successful", token });
                } else {
                    res.status(401).json({ status: 0, message: "Incorrect Credentials" });
                }
            });
        }else{
            res.status(404).json({ status: 0, message: "No user Record Found" });
        }
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

exports.toggleUser = (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    const id = req.params.id;
    User.findByPk(id).then(result=>{
        if(result.isEnable){
            var userToggles = { isEnable: 0 };
            User.update(userToggles, { returning: true, where: { id } })
            .then(data => {
                res.json({
                    status: 1,
                    message: "User Disabled"
                });
            })
            .catch(err => {
                res.status(400).json({status: -1, message: "error occured", error: err });
            });
        }else{
            var userToggles = { isEnable: 1, isVerify:1 };
            User.update(userToggles, { returning: true, where: { id } })
            .then(data => {
                res.json({
                    status: 1,
                    message: "User Enabled"
                });
            })
            .catch(err => {
                res.status(400).json({status: -1, message: "error occured", error: err });
            });
        }
    }).catch(err => {
        res.json({
            status: -1,
            message: "error occured",
            error: err
        });
    });
};

//filter memberType
exports.getAllmemberType =async  (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    try {
        var result='';
        if (req.params.memberType=="individual") {
             result=await User.findAll({ where:  {memberType: "individual",userType:"user" } });
        }else if(req.params.memberType=="organisation"){
             result=await User.findAll({ where:  {memberType: "organisation",userType:"user" } });
        }else{
             result=await User.findAll({ where:  {organisationType: req.params.memberType } });
        }
        await res.json({status: 1, message: `All  ${req.params.memberType} data Fetched Successfully`, data: result});
    } catch (error) {
        res.json({status: -1,message: "error occured",error: error});
    }
};