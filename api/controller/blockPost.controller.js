const db = require("../model/index");
const BlockPost = db.blockPost;
const Op = db.Sequelize.Op;



//Add BlockPost
exports.addBlockPost = (req, res) => {
    req.body.userId=req.userId;
    BlockPost.create(req.body).then(data => {
        res.json({ status: 1, message: "BlockPost Added Successfully." });
    }).catch(err => {
        res.status(400).json({status: -1, message: "Error occured while Adding BlockPost.", error: err });
    });
};

//Get All BlockPost
exports.getAllBlockPost = (req, res) => {
    BlockPost.findAll().then(result => {
        res.json({
            status: 1,
            message: "All BlockPost fetched successfully",
            data: result
        });
    }).catch(err => {
        res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

//Get BlockPost by Id
exports.getBlockPostById = (req, res) => {
    const id = req.params.id;

    BlockPost.findByPk(id)
        .then(result => {
            res.json({
                status: 1,
                message: "BlockPost by Id fetched successfully",
                data: result
            });
        }).catch(err => {
            res.status(400).json({
        status: -1,
                message: "error occured",
                error: err
            });
        });
};

///Update BlockPost
exports.updateBlockPost = (req, res) => {
    const id = req.params.id;
    req.body.userId=req.userId;
    BlockPost.update(req.body, { returning: true, where: { id } })
        .then(data => {
            res.json({
                status: 1,
                message: "BlockPost Updated"
            });
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};

//Delete BlockPost
exports.deleteBlockPost = (req, res) => {
    const id = req.params.id;

    BlockPost.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.json({
                    status: 1,
                    message: "BlockPost was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete BlockPost with id=${id}. Maybe BlockPost was not found!`
                });
            }
        })
        .catch(err => {
            res.status(400).json({status: -1, message: "error occured", error: err });
        });
};