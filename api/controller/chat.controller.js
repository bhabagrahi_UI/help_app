const db = require("../model/index");
const ChatRoom = db.chatRoom;
const ChatMessage = db.chatMessage;
const Op = db.Sequelize.Op;

const sequelize = db.sequelize;
const uuid = require("uuid");

exports.createRoom = (req, res) => {
  if(req.userId === req.body.userId){
    return res.json({status: 0, message: 'Cannot create a chat with self'});
  }
  const id = uuid.v4();
  const room1 = {
    uuid: id,
    user: req.body.userId,
  };
  const room2 = {
    uuid: id,
    user: req.userId,
  };
  ChatRoom.bulkCreate([room1, room2])
    .then((data) => {
      res.json({
        status: 1,
        message: "Rooms Added Successfully.",
        data: { roomID: id },
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "Error occured while Adding Rooms.",
        error: err,
      });
    });
};

//Add Comment
exports.addChat = (req, res) => {
  const message = {
    uuid: req.body.roomId,
    sentBy: req.userId,
    message: req.body.message,
  };
  ChatMessage.create(message)
    .then((data) => {
      res.json({ status: 1, message: "Chat Added Successfully." });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "Error occured while Adding Chat.",
        error: err,
      });
    });
};

//Get All Chats
exports.getAllChatRooms = (req, res) => {
  ChatRoom.findAll({ where: { user: req.userId } })
    .then((result) => {
      res.json({
        status: 1,
        message: "All Comment fetched successfully",
        data: result,
      });
    })
    .catch((err) => {
      res.status(400).json({status: -1, message: "error occured", error: err });
    });
};

//Get All Chat
exports.getChatData = (req, res) => {
  ChatMessage.findAll({
    where: { uuid: req.params.uuid },
    order: [["createdAt", "DESC"]],
    limit: 10,
    offset: (req.query.chunk|0)
  })
    .then((result) => {
      res.json({
        status: 1,
        message: "All Chat fetched successfully",
        maxId: result[0].Id,
        data: result,
      });
    })
    .catch((err) => {
      res.status(400).json({status: -1, message: "error occured", error: err });
    });
};
