const db = require("../model/index");
const SocialPost = db.socialPost;
const User = db.user;
const Tag = db.tag;
const Like = db.like;
const Comment = db.comment;
// const Op = db.Sequelize.Op;
const { Op } = require("sequelize");

const sequelize = db.sequelize;

//Add SocialPost
exports.addSocialPost = (req, res) => {
  var fullUrl = req.protocol + "://" + req.get("host");
  let thumb, images = [];
  if (req.files.thumb) {
    thumb =fullUrl+ "/image/" + req.files.thumb[0].filename;
    // thumb = "images/" + req.files.thumb.name;
  }
  if (req.files.images) {
    for (let img of req.files.images) {
      // console.log(img);
      images.push(fullUrl+"/image/" + img.filename);
    }
    // if(req.files.images.length){
    //   for (let img of req.files.images) {
    //     images.push("images" + img.name);
    //   }
    // }else{
    //   images = "images/" + req.files.images.name;
    // }    
  }

  //added missing data values for tagPeople
  const people = JSON.parse(req.body.tagPeople);
  for (let user of people) {
    user.createdBy = req.userId;
    user.updatedBy = req.userId;
  }

  const data = {
    userId: req.userId,
    title: req.body.title,
    caption: req.body.caption,
    hashtag: req.body.hashtag,
    postType: req.body.postType,
    location: req.body.location,
    category: req.body.category,
    tagPeople: people,
    thumb,
    images: JSON.stringify(images),
  };
  // return res.end(data);
  //added missing data values for tagPeople
  SocialPost.create(data, {
    include: ["tagPeople"],
  })
    .then((data) => {
      res.json({ status: 1, message: "SocialPost Added." });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "Error occured while Adding SocialPost.",
        err,
      });
    });
};

//Get All SocialPost
exports.getAllSocialPost = (req, res) => {
  SocialPost.findAll({
    include: [
      "tagPeople",
      {
        model: Like,
        as: "likes",
        include: [
          {
            model: User,
            as: "user",
            // include: ["chargeHead", "chargeCurrency"],
          },
        ],
      },
      {
        model: Comment,
        as: "comments",
        include: [
          {
            model: User,
            as: "user",
            // include: ["chargeHead", "chargeCurrency"],
          },
        ],
      },
      // "likes",
      // "comments"
    ],
  })
    .then((result) => {
      res.json({
        status: 1,
        message: "All SocialPost fetched successfully",
        data: result,
      });
    })
    .catch((err) => {
      res.status(400).json({ status: -1, message: "error occured" });
    });
};

//Get SocialPost by Id
exports.getSocialPostById = (req, res) => {
  const id = req.params.id;

  SocialPost.findByPk(id, { include: ["tagPeople"] })
    .then((result) => {
      res.json({
        status: 1,
        message: "SocialPost by Id fetched successfully",
        data: result,
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "error occured",
      });
    });
};

///Update SocialPost
exports.updateSocialPost = async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  const id = req.params.id;

  const t = await sequelize.transaction();
  try {
    var newSocialPost = await SocialPost.update(
      req.body,
      { returning: true, where: { id } },
      { transaction: t }
    );
    console.log(newSocialPost);
    //For Tag
    if (req.body.tagPeople.length > 0) {
      req.body.tagPeople.forEach((data, key) => {
        data.updatedBy = req.body.updatedBy;
        data.createdBy = 1;
        data.postId = id;
      });
      var deleteTag = await Tag.destroy(
        { where: { postId: id } },
        { transaction: t }
      );
      var newTag = await Tag.bulkCreate(req.body.tagPeople, { transaction: t });
    } else if (req.body.tagPeople.length == 0) {
      var deleteTag = await Tag.destroy(
        { where: { postId: id } },
        { transaction: t }
      );
    }
    await t.commit();
    res.json({ status: 1, message: "SocialPost updated." });
  } catch (error) {
    console.log(error);
    await t.rollback();
    res.status(400).json({
      status: -1,
      message: "Error occured while update SocialPost",
    });
  }
};

exports.uploadSocialPostImage = async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  let image;
  let uploadPath;

  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send("No files were uploaded.");
  } else if (!req.files.image) {
    res.json({ status: 1, message: "please check your inputs." });
  } else {
    var date = Date.now();
    image = req.files.image;
    uploadPath = process.cwd() + "/uploads/socialpost/" + date + image.name;
    await image.mv(uploadPath);
    var fullUrl = req.protocol + "://" + req.get("host");
    res.json({
      status: 1,
      message: "Image upload Successfully.",
      image: fullUrl + `/image/socialpost/${date + image.name}`,
    });
  }
};

//Delete SocialPost
exports.deleteSocialPost = async (req, res) => {
  const id = req.params.id;

  const t = await sequelize.transaction();
  try {
    await Tag.destroy({ where: { postId: id } }, { transaction: t });
    await SocialPost.destroy({ where: { id: id } }, { transaction: t });

    await t.commit();
    await res.json({
      status: 1,
      message: "SocialPost deleted Successfully.",
    });
  } catch (error) {
    await t.rollback();
    await res.status(400).json({
      status: -1,
      message: "Error occured while deleting SocialPost",
    });
  }
};

//Get All Post By UserId
exports.getAllSocialPostByUserId = async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  const userId = req.userId;
  const t = await sequelize.transaction(); // use for async operation
  // console.log(userId);
  try {
    var user = await User.findByPk(userId, { transaction: t });
    var whereStatement = {};
    if (user.profileType == "needy") {
      whereStatement = { postType: "helper", isActive: 1 };
    } else if (user.profileType == "helper") {
      whereStatement = { postType: "needy", isActive: 1 };
    } else {
      whereStatement = { isActive: 1 };
    }
    var sdata = await SocialPost.findAll({
      where: whereStatement,
      include: ["user"],
    });
    res.json({
      status: 1,
      message: "All Post fetched successfully",
      data: sdata,
    });
  } catch (error) {
    await t.rollback();
    res
      .status(400)
      .json({ status: -1, message: "Error occured while Fetching Data" });
  }
};

//Filter Post
exports.filterPosts = async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  const t = await sequelize.transaction(); // use for async operation
  try {
    var whereStatement = {};
    if (req.body.profileType && req.body.status) {
      whereStatement = {
        postType: req.body.profileType,
        statusType: req.body.status,
        isActive: 1,
      };
      var filterdata = await SocialPost.findAll({
        where: whereStatement,
        include: ["user"],
      });
      res.json({
        status: 1,
        message: "Posts filtered successfully",
        data: filterdata,
      });
    } else {
      // var data=await SocialPost.findAll({include: ["user"]});
      res.json({
        status: 1,
        message: "Posts filtered successfully...",
        data: [],
      });
    }
  } catch (error) {
    await t.rollback();
    res
      .status(400)
      .json({ status: -1, message: "Error occured while Fetching Data" });
  }
};

//search Post
exports.searchPosts = async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  const t = await sequelize.transaction(); // use for async operation
  try {
    var whereStatement = {};

    if (req.body.search == "needy" || req.body.search == "helper") {
      whereStatement = { isActive: 1, postType: req.body.search };
      console.log(whereStatement);
      var filterdata = await SocialPost.findAll({
        where: whereStatement,
        include: ["tagPeople"],
      });
      res.json({
        status: 1,
        message: "Search Posts Updated successfully",
        data: filterdata,
      });
    } else if (req.body.search == "") {
      res.json({ status: 0, message: "Provide All FIlter Parameters" });
    } else if (req.body.search != "needy" || req.body.search != "helper") {
      whereStatement = {
        isActive: 1,
        [Op.or]: [{ title: { [Op.substring]: req.body.search } }],
      };
      console.log(whereStatement);
      var filterdata1 = await SocialPost.findAll({
        where: whereStatement,
        include: ["tagPeople"],
      });
      res.json({
        status: 1,
        message: "Search Posts Updated successfully...",
        data: filterdata1,
      });
    }
  } catch (error) {
    await t.rollback();
    res
      .status(400)
      .json({ status: -1, message: "Error occured while Fetching Data" });
  }
};
