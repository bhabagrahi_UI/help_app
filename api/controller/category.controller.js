const db = require("../model/index");
const Category = db.category;
// const Op = db.Sequelize.Op;
const { Op } = require("sequelize");

const sequelize = db.sequelize;

//Add Category
exports.addCategory = (req, res) => {
  const data = {
    name: req.body.name,
    type: req.body.type,
    isActive: 1,
  };
  Category.create(data)
    .then((data) => {
      res.json({ status: 1, message: "Category Added." });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "Error occured while Adding Category.",
        err,
      });
    });
};

//Remove Category
exports.removeCategory = (req, res) => {
  Category.update(
    {
      isActive: req.body.isActive,
    },
    { where: { id: req.body.id } }
  )
    .then((data) => {
      res.json({ status: 1, message: "Category Updated." });
    })
    .catch((err) => {
      res.status(400).json({
        status: -1,
        message: "Error occured while Updating Category.",
        err,
      });
    });
};

//Get All Categories
exports.getAllCategories = (req, res) => {
  Category.findAll({
    where: { isActive: 1 },
  })
    .then((result) => {
      res.json({
        status: 1,
        message: "All Categories fetched successfully",
        data: result,
      });
    })
    .catch((err) => {
      res.status(400).json({ status: -1, message: "error occured" });
    });
};
