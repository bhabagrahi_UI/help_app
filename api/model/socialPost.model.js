module.exports = (sequelize, Sequelize) => {
    const SocialPost = sequelize.define("tbl_social_post", {
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        postType: {
            type: Sequelize.ENUM('needy','helper'),
            allowNull: false,
        },
        caption: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        title: {
            type: Sequelize.STRING(150),
            allowNull: false,
        },
        hastag: {
            type: Sequelize.TEXT,
            allowNull: true,
        },
        location: {
            type: Sequelize.TEXT,
            allowNull: true,
        },
        fundraiser: {
            type: Sequelize.BIGINT,
            allowNull: false,
            defaultValue: 0
        },
        crowdfunding: {
            type: Sequelize.BIGINT,
            allowNull: false,
            defaultValue: 0
        },
        reasonForCrowdfunding: {
            type: Sequelize.TEXT,
        },
        // link: {
        //     type: Sequelize.STRING(100),
        //     // unique: true
        // },
        video: {
            type: Sequelize.TEXT,
            allowNull: true,
        },
        thumb: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        images: {
            type: Sequelize.TEXT,
            allowNull: true,
        },
        category: {
            type: Sequelize.ENUM('Category1','Category2','Category3','Category4','Category5'),
            allowNull: false,
        },
        isPriority: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: 0,
            comment: "0= normal, 1= high Alerts"
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        statusType: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return SocialPost;
};

