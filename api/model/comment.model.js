module.exports = (sequelize, Sequelize) => {
    const Comment = sequelize.define("tbl_comment", {
        parentId: {
            type: Sequelize.INTEGER,
            allowNull: false, 
            defaultValue: 0                    
        },
        postId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_social_post",
                key: "id"
            },
            allowNull: false,
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        comment: {
            type: Sequelize.TEXT,
            allowNull: false, 
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Comment;
};

