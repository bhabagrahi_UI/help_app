module.exports = (sequelize, Sequelize) => {
    const Tag = sequelize.define("tbl_tag", {
        postId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_social_post",
                key: "id"
            },
            allowNull: false,
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        isTag: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Tag;
};

