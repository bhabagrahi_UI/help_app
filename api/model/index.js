const dbConfig = require("../config/sequelize.config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model")(sequelize, Sequelize);
db.socialPost = require("./socialPost.model")(sequelize, Sequelize);
db.like = require("./like.model")(sequelize, Sequelize);
db.comment = require("./comment.model")(sequelize, Sequelize);
db.tag = require("./tag.model")(sequelize, Sequelize);
db.blockPost = require("./blockPost.model")(sequelize, Sequelize);
db.feedback = require("./feedback.model")(sequelize, Sequelize);
db.chatRoom = require("./chatRoom.model")(sequelize, Sequelize);
db.chatMessage = require("./chatMessage.model")(sequelize, Sequelize);
db.notification = require("./notification.model")(sequelize, Sequelize);
db.category = require("./category.model")(sequelize, Sequelize);




db.user.hasMany(db.socialPost, { as: "socialPost", foreignKey: "userId" }); //Add One to Many

db.socialPost.belongsTo(db.user, { as: "user", foreignKey: "userId" });
db.socialPost.hasMany(db.tag, { as: "tagPeople", foreignKey: "postId" }); //Add One to Many
db.socialPost.hasMany(db.like, { as: "likes", foreignKey: "postId" }); //Add One to Many
db.socialPost.hasMany(db.comment, { as: "comments", foreignKey: "postId" }); //Add One to Many

db.like.belongsTo(db.user, { as: "user", foreignKey: "userId" });
db.comment.belongsTo(db.user, { as: "user", foreignKey: "userId" });








////////****Not For USE */
//Associations for quotation
// db.quotation.belongsTo(db.customer, { as: "customer" });
// db.quotation.belongsTo(db.serviceType, { as: "serviceType" });
// db.quotation.belongsTo(db.addressBook, { as: "vendor" });
// db.quotation.belongsTo(db.currencyType, { as: "currency" });

// db.quotation.hasMany(db.quotationCharges, { as: "quotationCharges", foreignKey: "quotationId" }); //Add One to Many
// db.quotation.hasMany(db.quotationMovement, { as: "quotationMovement", foreignKey: "quotationId" }); //Add One to Many


//Associations for city
// db.city.belongsTo(db.country, { as: "country", foreignKey: "countryId" });
// db.city.belongsTo(db.state, { as: "state", foreignKey: "stateId"});


// db.contract.hasMany(db.contractCost, { as: "contractCost", foreignKey: "contractId" }); //Add One to Many



module.exports = db;