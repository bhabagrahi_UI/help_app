module.exports = (sequelize, Sequelize) => {
    const Chatroom = sequelize.define("tbl_chat_room", {
        Id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        uuid: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        user: {
            type: Sequelize.INTEGER,
            allowNull: false, 
            references: {
                model: "tbl_user",
                key: "id"
            },                  
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Chatroom;
};

