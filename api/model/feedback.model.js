module.exports = (sequelize, Sequelize) => {
    const Feedback = sequelize.define("tbl_feedback", {
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        receivedId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        ratings: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        feedbacks: {
            type: Sequelize.TEXT,
            allowNull: false, 
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Feedback;
};

