module.exports = (sequelize, Sequelize) => {
    const Notification = sequelize.define("tbl_notification", {
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        notificationMsg: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        notificationevent: {
            type: Sequelize.STRING(250),
            allowNull: false,
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Notification;
};

