module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("tbl_category", {
        name: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        type:{
            type: Sequelize.TEXT,
            allowNull: false,
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Category;
};

