module.exports = (sequelize, Sequelize) => {
    const Block = sequelize.define("tbl_block_post", {
        postId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_social_post",
                key: "id"
            },
            allowNull: false,
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: "tbl_user",
                key: "id"
            },
            allowNull: false,
        },
        isBlock: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        },
        isReportToAdmin: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '0'                    
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: '1'                    
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return Block;
};

