const bcrypt = require("bcrypt");
module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("tbl_user", {
        userCode: {
            type: Sequelize.STRING(20),
            allowNull: true,
            unique: true
        },
        profileType: {
            type: Sequelize.ENUM('needy','helper','awareness'),
            allowNull: true,
        },
        userType: {
            type: Sequelize.ENUM('superAdmin','admin','user'),
            allowNull: false,
            defaultValue:"user"
        },
        memberType: {
            type: Sequelize.ENUM('individual','organisation'),
            allowNull: false,
            defaultValue:"individual"
        },
        organisationType: {
            type: Sequelize.ENUM('ngo','charity trust','orphanage','old-age home','donation box'),
            allowNull: true,
            // defaultValue:"Individual"
        },
        name: {
            type: Sequelize.STRING(100),
            allowNull: true,
        },
        email: {
            type: Sequelize.STRING(100),
            allowNull: true,
            unique: true
        },
        phone: {
            type: Sequelize.STRING(25),
            allowNull: true,
            unique: true
        },
        password: {
            type: Sequelize.STRING(250),
            allowNull: false,
            set(value) {
                // Storing passwords in plaintext in the database is terrible.
                // Hashing the value with an appropriate cryptographic hash function is better.
                this.setDataValue('password', bcrypt.hashSync(value, 9));
            }
        },
        profilePicture: {
            type: Sequelize.TEXT,
            allowNull: true,
        },
        address: {
            type: Sequelize.TEXT,
            allowNull: true, 
        },
        latitude: {
            type: Sequelize.STRING(45),
            allowNull: true,
        },
        longitude: {
            type: Sequelize.STRING(45),
            allowNull: true,
        },
        gender: {
            type: Sequelize.STRING(30),
            allowNull: true,
        },
        profession: {
            type: Sequelize.STRING(100),
            allowNull: true,
        },
        dob: {
            type: Sequelize.DATEONLY,
            allowNull: true,
        },
        desc: {
            type: Sequelize.TEXT,
            allowNull: true, 
        },
        otp: {
            type: Sequelize.STRING(10),
            allowNull: true, 
            defaultValue: "0000"
        },
        signupMethod: {
            type: Sequelize.STRING(100),
            allowNull: true, 
            // defaultValue: 'phone' 
        },
        isEnable: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: 0                    
        },
        isVerify: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: 0                    
        },
        isPublic: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: 1                    
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            allowNull: false, 
            defaultValue: 0                    
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: '1'
        },
        updatedBy: {
            type: Sequelize.INTEGER,
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return User;
};

