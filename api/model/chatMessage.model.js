module.exports = (sequelize, Sequelize) => {
    const ChatMessage = sequelize.define("tbl_chat_message", {
        Id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        uuid: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        sentBy: {
            type: Sequelize.INTEGER,
            allowNull: false, 
            references: {
                model: "tbl_user",
                key: "id"
            },                  
        },
        message:{
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    return ChatMessage;
};

