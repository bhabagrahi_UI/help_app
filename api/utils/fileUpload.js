const multer = require('multer')
const fs = require('fs');
const path = require('path');

let upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, callback) => {
            console.log(__dirname);
            let dest = path.join(__dirname, '../../', `/uploads`);
            if (!fs.existsSync(dest)) {
                fs.mkdirSync(dest);
            }
            callback(null, dest);
            // fs.mkdir(dest, function (err) {
            //     if (err) {
            //         return console.error(err);
            //     }
            //     callback(null, dest);
            // });
            // fs.mkdirsSync(path);
        },
        filename: (req, file, callback) => {
            //originalname is the uploaded file's name with extn
            callback(null, new Date().getTime() + file.originalname);
        }
    })
});

module.exports = upload;