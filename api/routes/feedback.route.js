module.exports = app => {
    const feedback = require("../controller/feedback.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/feedback/addFeedback",token, feedback.addFeedback);
      
    // Retrieve all 
    app.get("/api/v1/rest/feedback/getAllFeedback", feedback.getAllFeedback);
  
    // Retrieve a single
    app.get("/api/v1/rest/feedback/getFeedbackById/:id", feedback.getFeedbackById);
  
    // Update
    app.put("/api/v1/rest/feedback/updateFeedback/:id",token, feedback.updateFeedback);
  
    // Delete
    app.delete("/api/v1/rest/feedback/deleteFeedback/:id", feedback.deleteFeedback);
  
  
};