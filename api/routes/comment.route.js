module.exports = app => {
    const comment = require("../controller/comment.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/comment/addComment",token, comment.addComment);
      
    // Retrieve all 
    app.get("/api/v1/rest/comment/getAllComment", comment.getAllComment);
  
    // Retrieve a single
    app.get("/api/v1/rest/comment/getCommentById/:id", comment.getCommentById);
  
    // Update
    app.put("/api/v1/rest/comment/updateComment/:id",token, comment.updateComment);
  
    // Delete
    app.delete("/api/v1/rest/comment/deleteComment/:id", comment.deleteComment);
  
  
};