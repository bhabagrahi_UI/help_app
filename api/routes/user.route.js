
module.exports = app => {
    const user = require("../controller/user.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/user/addUser", user.addUser);

    //user login
    app.post("/api/v1/rest/user/login", user.login);

    //generate OTP
    app.post("/api/v1/rest/user/otp", user.otp);

    //verify OTP
    app.post("/api/v1/rest/user/verifyotp", user.verifyotp);
  
    // Update
    app.put("/api/v1/rest/user/updateUser",token, user.editUser);

    app.put("/api/v1/rest/user/addProfileType/:id", user.editProfileType);

    //Add Profile Pic
    app.post("/api/v1/rest/user/addProfilePic",token, user.adddProfilePic);

    //Forgot password
    app.post("/api/v1/rest/user/forgotPassword", user.forgotPassword);

    //Change password
    app.post("/api/v1/rest/user/changePassword",token, user.changePassword);

    // Retrieve all USER
    app.get("/api/v1/rest/user/getAllUser", user.getAllUser);

      
    // Retrieve a single
    // app.get("/api/v1/user/getUserById/:id", user.getUserById);
  
    // Delete
    app.delete("/api/v1/rest/user/removeUser",token, user.removeUser);
    app.delete("/api/v1/rest/user/deleteUser",token, user.deleteUser);

  };