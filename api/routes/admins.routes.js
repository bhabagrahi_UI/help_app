module.exports = app =>{
    const adminuser = require("../controller/admins.controller");
    const category = require("../controller/category.controller");
    const user = require("../controller/user.controller");
    const socialPost = require("../controller/socialPost.controller");
    // const token = require("../utils/veifyToken");

    //Admin login
    app.post("/api/v1/rest/admin/user/adminlogin", adminuser.adminLogin);

    // Retrieve all USER
    app.get("/api/v1/rest/admin/user/getAllUser", user.getAllUser);

    // Toggle USER BY user ID
    app.get("/api/v1/rest/admin/user/toggleUser/:id", adminuser.toggleUser);

    // Retrieve all Social Post
    app.get("/api/v1/rest/admin/post/getAllSocialPost", socialPost.getAllSocialPost);

    // Retrieve all memberType (individual or organisation)
    app.get("/api/v1/rest/admin/user/getAllMemberType/:memberType", adminuser.getAllmemberType);

    // Edit User
    app.put("/api/v1/rest/admin/user/updateUser/:id", user.editUser);

    // Edit User
    app.put("/api/v1/rest/admin/socialPost/updateSocialPost/:id", socialPost.updateSocialPost);

    app.post("/api/v1/rest/admin/category/add", category.addCategory);

    app.post("/api/v1/rest/admin/category/remove", category.removeCategory);

    app.get("/api/v1/rest/admin/category/fetch", category.getAllCategories);

}