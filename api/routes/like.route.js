module.exports = app => {
    const like = require("../controller/like.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/like/addLike",token, like.addLike);
      
    // Retrieve all 
    app.get("/api/v1/rest/like/getAllLike", like.getAllLike);
  
    // Retrieve a single
    app.get("/api/v1/rest/like/getLikeById/:id", like.getLikeById);
  
    // Update
    app.put("/api/v1/rest/like/updateLike/:id",token, like.updateLike);
  
    // Delete
    app.delete("/api/v1/rest/like/deleteLike/:id", like.deleteLike);
  
  
};