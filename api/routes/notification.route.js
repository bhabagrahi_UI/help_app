module.exports = app => {
    const notification = require("../controller/notification.controller");
    const token = require("../utils/veifyToken");

    app.use(token);

    // Create a new 
    // app.post("/api/v1/rest/notification/addNotification", notification.addNotification);
      
    // Retrieve all 
    app.get("/api/v1/rest/notification/getNotifications", notification.getAllNotificationByUserId);
  
    // Retrieve a single
    // app.get("/api/v1/rest/notification/getNotificationById/:id", notification.getNotificationById);
  
    // Update
    // app.put("/api/v1/rest/notification/updateNotification/:id", notification.updateNotification);
  
    // Delete
    app.delete("/api/v1/rest/notification/deleteNotification/:id", notification.deleteNotification);

    // Clear Notofication
    app.delete("/api/v1/rest/notification/clearNotification", notification.clearNotification);
  
  
};