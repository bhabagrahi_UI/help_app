module.exports = app => {
    const blockPost = require("../controller/blockPost.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/blockPost/addBlockPost",token, blockPost.addBlockPost);
      
    // Retrieve all 
    app.get("/api/v1/rest/blockPost/getAllBlockPost", blockPost.getAllBlockPost);
  
    // Retrieve a single
    app.get("/api/v1/rest/blockPost/getBlockPostById/:id", blockPost.getBlockPostById);
  
    // Update
    app.put("/api/v1/rest/blockPost/updateBlockPost/:id",token, blockPost.updateBlockPost);
  
    // Delete
    app.delete("/api/v1/rest/blockPost/deleteBlockPost/:id", blockPost.deleteBlockPost);
  
  
};