

module.exports = app => {
    const socialPost = require("../controller/socialPost.controller");
    const token = require("../utils/veifyToken");
    
    // Create a new 
    app.post("/api/v1/rest/socialPost/addSocialPost",token, socialPost.addSocialPost);
      
    // Retrieve all 
    app.get("/api/v1/rest/socialPost/getAllSocialPost", socialPost.getAllSocialPost);
  
    // Retrieve a single
    app.get("/api/v1/rest/socialPost/getSocialPostById/:id", socialPost.getSocialPostById);

    // Retrieve a Data By userId
    app.get("/api/v1/rest/socialPost/getSocialPostByUserId",token, socialPost.getAllSocialPostByUserId);

    // Filter post Data By userId
    app.post("/api/v1/rest/socialPost/filterSocialPost", socialPost.filterPosts);

    // Search post Data By userId
    app.post("/api/v1/rest/socialPost/searchSocialPost", socialPost.searchPosts);

    // Upload Image
    app.post("/api/v1/rest/socialPost/uploadImage", socialPost.uploadSocialPostImage);
  
    // Update
    app.put("/api/v1/rest/socialPost/updateSocialPost/:id", socialPost.updateSocialPost);
  
    // Delete
    app.delete("/api/v1/rest/socialPost/deleteSocialPost/:id", socialPost.deleteSocialPost);
  
  };