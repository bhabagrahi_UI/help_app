// const veifyToken = require("../utils/veifyToken");

module.exports = app => {
    // const user = require("../controller/user.controller");
    const chat = require("../controller/chat.controller");
    const token = require("../utils/veifyToken");
    
    // app.use(veifyToken);

    //Creation of Chat Rooms -> userID of to user 
    app.post("/api/v1/rest/user/createRoom",token, chat.createRoom);

    //Add Messages to ChatRoom -> RoomID, message
    app.post("/api/v1/rest/user/addMessage",token, chat.addChat);

    //get All ChatRooms of an user
    app.get("/api/v1/rest/user/getChatRooms",token, chat.getAllChatRooms);
    
    //get Chatroom messages of a chatroom -> query: pageNo(default to 0)  params: chatroomID
    app.get("/api/v1/rest/user/getChats/:uuid",token, chat.getChatData);

    
}