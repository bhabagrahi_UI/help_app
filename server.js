'use strict';
const express = require('express');
const app = express();
require ('dotenv').config();
const cors = require("cors");
const helmet = require("helmet");
const upload = require('./api/utils/fileUpload');

app.use(express.json()); //parse request Data to json
app.use('/image',express.static('uploads'))

// Used this block of code to allow CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

app.use(helmet());
app.use(cors());
app.options("*", cors());

const db = require("./api/config/db.config");

//////////////////////*****Sequelize Start*****//////////////////////////
const seq = require("./api/model");

// seq.sequelize.sync({ force: false }).then(() => {
//     console.log("Drop and re-sync db.");
// });
//////////////////////*****Sequelize End*****//////////////////////////
app.use(upload.fields([{
  name: 'thumb', maxCount: 1
}, {
  name: 'images', maxCount: 3
}]))
require("./api/routes/user.route")(app); //Importing User Routes
require("./api/routes/socialPost.route")(app); //Importing SocialPost Routes
require("./api/routes/like.route")(app); //Importing Like Routes
require("./api/routes/comment.route")(app); //Importing Comment Routes
require("./api/routes/blockPost.route")(app); //Importing blockPost Routes
require("./api/routes/feedback.route")(app); //Importing feedback Routes
require("./api/routes/chat.route")(app); //Importing chat Routes
require("./api/routes/admins.routes")(app); //Importing Admin Routes
require("./api/routes/notification.route")(app); //Importing Notification Routes


app.get('*', function (req, res) {
  res.status(404).send('Page Not Found');
});

var server = app.listen(process.env.PORT || '3000', function () {
  console.log('Server running on port ', server.address().port);
});

module.exports = app;
